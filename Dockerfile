# 
# Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
# Description: 
# Generic container file for nodejs container
# Arguments:
#  - NODE_VERSION=Node's base container version number (default: 12.16.2-slim)
#  - NODE_ENV=production|development (default: production)
#  - PORT=Express listener port on which traffic will be exposed
ARG NODE_VERSION=14.15.3-slim


#
# Builder container
#
FROM node:${NODE_VERSION} AS builder

# Creating application directory
RUN mkdir /opt/node_app && chown -R node:node /opt/node_app
WORKDIR /opt/node_app

# Install @angular/cli in global
# BUG! Should find a way to install it locally
RUN npm install -g @angular/cli &&\
      npm install -g lite-server

USER node
COPY --chown=node:node  *  ./
COPY --chown=node:node  ./src src/


RUN npm config set scripts-prepend-node-path auto &&\
      npm install --force &&\
      ng build 

##
## Main Container
##
FROM node:${NODE_VERSION}

LABEL "authur"="openfintechlab.com" \
      "source-repo"="https://gitlab.com/openfintechlab/medulla/console/medmgmtconsole" \
      "copyright"="Copyright 2020-2022 Openfintechlab, Inc. All rights reserved."

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

# default to port 3000 for node
ARG PORT=3000
ENV PORT $PORT 

# Creating application directory
RUN mkdir /opt/node_app && \
      mkdir /opt/node_app/bin && \            
      chown -R node:node /opt/node_app && \
      npm install -g lite-server    

WORKDIR /opt/node_app

# Using non-root user shipped with the baseline container
USER node

COPY --chown=node:node --from=builder  /opt/node_app/dist ./dist

# When creating an image, you can bypass the package.json's start command and bake it directly 
# into the image itself. First off this reduces the number of processes running inside of your 
# container. Secondly it causes exit signals such as SIGTERM and SIGINT to be received by the 
# Node.js process instead of npm swallowing them.
CMD [ "lite-server", "--baseDir=/opt/node_app/dist/medulla/" ]

# References:
# - https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md#non-root-user
# - https://github.com/BretFisher/node-docker-good-defaults/blob/master/Dockerfile
# - https://docs.docker.com/develop/develop-images/multistage-build/ 
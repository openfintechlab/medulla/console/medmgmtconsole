import { Component, OnInit, ViewChild } from '@angular/core';
import { SidenavService } from 'src/app/services/layout/sidenav.service';
import { MatSidenav } from '@angular/material/sidenav';
import {RouterOutlet } from '@angular/router';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @ViewChild('drawer') public sidenav!: MatSidenav
  constructor(
    private sidenavService: SidenavService
  ) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
  }
}

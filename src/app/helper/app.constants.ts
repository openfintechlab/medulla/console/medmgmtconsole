import { environment } from '../../environments/environment';

export let layoutView = {
  isSideBarShow: true,
  isHeaderShow : true,
  isFooterShow :  true,
};

function createUrl(actionName: string): string {
  return `${environment.API_BASE_URL}${actionName}`;
}


export const appApiResources = {
  baseUrl: environment.API_BASE_URL,
  // siteURL: environment.SITE_BASE_URL,
  tokenvalidity : createUrl('auth/tokenvalidity'),
  login :  createUrl('/usermanagement/login'),
  logout :  createUrl('auth/signout'),
  forgotPassword :  createUrl('auth/forgot_password'),
  resetPassword :  createUrl('auth/reset_password'),
  service_master:{
   list:{
    url: createUrl('/service-master'),  
    timeout: 4000,  
  },
   create: {
    url: createUrl('/service-master'),  
    timeout: 5000,  
  },
   detail: {
    url: createUrl('/service-master'),  
    timeout: 4000,  
  },

  },
  service_version:{
   list:{
    url: createUrl(':32008/service-version'),  
    timeout: 4000,  
  },
   delete:{
    url: createUrl(':32008/service-version-ora/service-version/'),  
    timeout: 4000,  
  },
}
    
  

}
export const appRoutes = {
  root: '',
  signup: 'signup',
  login: 'login',
  notFound: 'notfound',
  unauthorize: '401',
  logout: 'logout',
  forgotPassword: 'forgot-password',
  resetPassword: 'reset_password',
  dashboard: 'dashboard'
};

export const appVariables = {
  userLocalStorage: 'currentUser',
  accessTokenServer: 'authorization',
  defaultContentTypeHeader: 'application/json',
  accessTokenLocalStorage: 'spendebtAccessToken',
  plaidAccessToken: 'plaidAccessToken'
};



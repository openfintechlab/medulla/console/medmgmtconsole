import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './helper/auth.guard';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';
import { AdminComponent } from './modules/dashboard/admin/admin.component';
import { LoginComponent } from './modules/login/login/login.component';
import { ServiceCreateComponent } from './modules/services/service-create/service-create.component';
import { ServiceUpdateComponent } from './modules/services/service-update/service-update.component';
import { ServicesListComponent } from './modules/services/services-list/services-list.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: '', component: AdminComponent, pathMatch: 'full', canActivate: [AuthGuard]  },
  { path: 'services', component: ServicesListComponent, pathMatch: 'full',   },
  { path: 'services-create', component: ServiceCreateComponent, pathMatch: 'full',   },
  { path: 'service-update/:id', component: ServiceUpdateComponent,    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export class serviceMaster{
    srv_id: number;
    serviceTitle: string;
    serviceName:string
    group_slug: string;
    group_name: string;
    hostname:  any;
    port: number;
    ctx_root_uri: string;
    sdk_doc_uri: string;
    api_doc_uri: string;
    tags: string;
    lifecycle: string;
    type: string;
    visibility: string;
    implementedType: string;
    implementedVersion: string;
    isContainerized: string; 
    repo_branch: string;
    repo_url: string;
    repo_tech: string;
    repo_type: string;

    discussion_chl_type: string;
    discussion_chl_url: string;

    service_owner: string;
    service_owner_id: string;
    service_owner_service_id: string;
    service_owner_name: string;
    service_owner_email_id: string;
    service_owner_esc_rank: string;
    dcu: any;
    trimDcu?: any;
    dcuVersion: any;
    trimDcuVersion: any;
    dcuICont: any;
    trimDcuCont: any;

    source_uri: any;
    split_source_uri: any;
    split_source_uri_type: any;
    split_source_uri_tech: any;
    split_source_uri_branch: any;
    split_source_uri_url: any;
    disChnlUri: any;
    split_disChnlUri: any;
    split_disChnlUri_type: any;
    split_disChnlUri_url: any;

    constructor(serviceMasterResp: any){
    this.serviceName = serviceMasterResp.name
    this.srv_id = serviceMasterResp.srv_id
    this.serviceTitle = serviceMasterResp.title
    this.group_slug = serviceMasterResp.group_slug
    this.group_name= serviceMasterResp.group_name
    this.hostname= serviceMasterResp.hostname
    this.port= serviceMasterResp.port
    this.ctx_root_uri= serviceMasterResp.ctx_root_uri
    this.sdk_doc_uri= serviceMasterResp.sdk_doc_uri
    this.api_doc_uri= serviceMasterResp.api_doc_uri
    this.tags= serviceMasterResp.tags
    this.lifecycle= serviceMasterResp.lifecycle
    this.type= serviceMasterResp.type
    this.visibility= serviceMasterResp.visibility

// spliting response into values
    this.dcu = serviceMasterResp.imp_tech_uri
    this.trimDcu = this.dcu.split(';')
    this.trimDcu = this.trimDcu[0].split(':')
    this.implementedType= this.trimDcu[1]
    // console.log(this.implementedType)

    this.dcuVersion = serviceMasterResp.imp_tech_uri
    this.trimDcuVersion = this.dcuVersion.split(';')
    this.trimDcuVersion = this.trimDcuVersion[1].split(':')
    this.implementedVersion= this.trimDcuVersion[1]
    
    this.dcuICont = serviceMasterResp.imp_tech_uri
    this.trimDcuCont = this.dcuICont.split(';')
    this.trimDcuCont = this.trimDcuCont[2].split(':')
    this.isContainerized = this.trimDcuCont[1]
    
  
    this.source_uri= serviceMasterResp.source_uri
    this.split_source_uri = this.source_uri.split(';')
    
    this.split_source_uri_type = this.split_source_uri[0].split(':')
    this.repo_type = this.split_source_uri_type[1]
    
    this.split_source_uri_tech = this.split_source_uri[1].split(':')
    this.repo_tech = this.split_source_uri_tech[1]
    
    this.split_source_uri_url = this.split_source_uri[2].split(':')
    this.repo_url = this.split_source_uri_url[1]
    
    this.split_source_uri_branch = this.split_source_uri[3].split(':')
    this.repo_branch = this.split_source_uri_branch[1]


    this.disChnlUri = serviceMasterResp.discussion_chl_uri
    this.split_disChnlUri = this.disChnlUri.split(';')


    this.split_disChnlUri_type = this.split_disChnlUri[0].split(':')
    this.discussion_chl_type = this.split_disChnlUri_type[1]
    
    this.split_disChnlUri_url = this.split_disChnlUri[1].split(':')
    this.discussion_chl_url = this.split_disChnlUri_url[1]


    }
  }

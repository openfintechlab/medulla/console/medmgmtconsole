import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from './layout/layout.module';
import { LoginModule } from './modules/login/login.module';
import { ModulesModule } from './modules/modules.module';
import { SidenavService } from './services/layout/sidenav.service';
import { HttpClientModule } from '@angular/common/http';
import { 
  NgxUiLoaderModule,
  NgxUiLoaderConfig,
  SPINNER,
  POSITION,
  PB_DIRECTION,
 } from 'ngx-ui-loader';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const ngxUiLoaderConfig: NgxUiLoaderConfig =

{
  "bgsColor": "red",
  "bgsOpacity": 0.5,
  "bgsPosition": "bottom-right",
  "bgsSize": 60,
  "bgsType": "ball-spin-clockwise",
  "blur": 5,
  "delay": 0,
  "fastFadeOut": true,
  "fgsColor": "#00e0ff",
  "fgsPosition": "center-center",
  "fgsSize": 60,
  "fgsType": "folding-cube",
  "gap": 24,
  "logoPosition": "center-center",
  "logoSize": 120,
  "logoUrl": "",
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(40, 40, 40, 0.8)",
  "pbColor": "red",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": true,
  "text": "",
  "textColor": "#FFFFFF",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 300
}
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ModulesModule,
    HttpClientModule,
    NgxUiLoaderModule,
    MatSnackBarModule
    
    
  ],
  providers: [
    SidenavService,
  ],
  bootstrap: [AppComponent,]
})
export class AppModule { }

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { appApiResources } from '../../helper/app.constants';
import { HttpHelper } from '../../helper/http.helper';

@Injectable({
  providedIn: 'root'
})
export class ServiceMasterDataService {

  private headers;
  private baseURL;
  baseURLVersion: { list: { url: string; timeout: number; }; delete: { url: string; timeout: number; }; };
  // baseURLVersion: { list: { url: string; timeout: number; }; };

  constructor(
    private http: HttpClient,
    private httpClient: HttpClient,
    private httpHelper: HttpHelper,
  
  ) {
    this.baseURL = appApiResources.service_master;
    this.baseURLVersion = appApiResources.service_version;
    this.headers = this.httpHelper.getHttpHeaders();

   }


  getAll(): Observable<any> {
    const endPoint = `${this.baseURL.list.url}`;
    return this.http.get(endPoint)
      .pipe(tap(data => (JSON.stringify(data))) , catchError(this.errorHandler));
  }
  getSingle(id:any): Observable<any> {
    const endPoint = `${this.baseURL.detail.url}`;
    return this.http.get(`${endPoint}/${id}`)
      .pipe(tap(data => (JSON.stringify(data))) , catchError(this.errorHandler));
  }
  getSingleVersion(id:any): Observable<any> {
    const endPoint = `${this.baseURLVersion.list.url}`;
    return this.http.get(`${endPoint}/${id}`)
      .pipe(tap(data => (JSON.stringify(data))) , catchError(this.errorHandler));
  }

  onCreate(model: any): Observable<any> {
    const endPoint = `${this.baseURL.create.url}`;
    return this.http.post(endPoint, model)
                    .pipe(tap(data => (JSON.stringify(data))), catchError(this.errorHandler));
  }
  onCreateVersion(model: any): Observable<any> {
    const endPoint = `${this.baseURLVersion.list.url}`;
    return this.http.post(endPoint, model)
                    .pipe(tap(data => (JSON.stringify(data))), catchError(this.errorHandler));
  }
  onDeleteVersion(ver_id,srv_id): Observable<any> {
    const endPoint = `${this.baseURLVersion.delete.url}`;
    return this.http.post(endPoint,`${ver_id}/${srv_id}`)
                    .pipe(tap(data => (JSON.stringify(data))), catchError(this.errorHandler));
  }
  onUpdate(model: any): Observable<any> {
    const endPoint = `${this.baseURL.create.url}`;
    return this.http.put(endPoint, model)
                    .pipe(tap(data => (JSON.stringify(data))), catchError(this.errorHandler));
  }


  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.error || 'Server Error');
  }
 
}

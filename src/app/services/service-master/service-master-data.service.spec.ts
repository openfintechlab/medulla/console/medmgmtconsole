import { TestBed } from '@angular/core/testing';

import { ServiceMasterDataService } from './service-master-data.service';

describe('ServiceMasterDataService', () => {
  let service: ServiceMasterDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceMasterDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

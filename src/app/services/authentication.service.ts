import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../models/user.model';
import { appApiResources } from '../helper/app.constants';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private baseURL;

  constructor(private http: HttpClient) { 
    
  this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
  this.currentUser = this.currentUserSubject.asObservable();
  this.baseURL = appApiResources.login;
  }

  





  public get currentUserValue(): User {
    return this.currentUserSubject.value;
}

login(username: string, password: string) {
  const endPoint = `${this.baseURL}`;
     return this.http.post<any>( endPoint, {"userDetails":{ username, password }})
        .pipe(map(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
            return user;
        }));
}

logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
}
}

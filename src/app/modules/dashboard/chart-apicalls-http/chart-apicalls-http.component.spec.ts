import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartApicallsHttpComponent } from './chart-apicalls-http.component';

describe('ChartApicallsHttpComponent', () => {
  let component: ChartApicallsHttpComponent;
  let fixture: ComponentFixture<ChartApicallsHttpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartApicallsHttpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartApicallsHttpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartTotalApicallsComponent } from './chart-total-apicalls.component';

describe('ChartTotalApicallsComponent', () => {
  let component: ChartTotalApicallsComponent;
  let fixture: ComponentFixture<ChartTotalApicallsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartTotalApicallsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartTotalApicallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

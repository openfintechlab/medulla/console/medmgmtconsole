import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartCpuUsageComponent } from './chart-cpu-usage.component';

describe('ChartCpuUsageComponent', () => {
  let component: ChartCpuUsageComponent;
  let fixture: ComponentFixture<ChartCpuUsageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartCpuUsageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartCpuUsageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

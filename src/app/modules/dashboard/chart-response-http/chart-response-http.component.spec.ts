import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartResponseHttpComponent } from './chart-response-http.component';

describe('ChartResponseHttpComponent', () => {
  let component: ChartResponseHttpComponent;
  let fixture: ComponentFixture<ChartResponseHttpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartResponseHttpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartResponseHttpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

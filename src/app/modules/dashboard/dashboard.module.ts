import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { TableListingComponent } from './table-listing/table-listing.component';

import {MatTableModule} from '@angular/material/table';
import { MatFormField, MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { BrowserModule } from '@angular/platform-browser';
import { DxChartModule } from 'devextreme-angular/ui/chart';
import { DxSelectBoxModule } from 'devextreme-angular';
import { ChartTotalApicallsComponent } from './chart-total-apicalls/chart-total-apicalls.component';
import { ChartApicallsHttpComponent } from './chart-apicalls-http/chart-apicalls-http.component';
import { ChartResponseHttpComponent } from './chart-response-http/chart-response-http.component';
import { ChartCpuUsageComponent } from './chart-cpu-usage/chart-cpu-usage.component';
import { ChartMemoryUsageComponent } from './chart-memory-usage/chart-memory-usage.component';

@NgModule({
  declarations: [AdminComponent, TableListingComponent, ChartTotalApicallsComponent, ChartApicallsHttpComponent, ChartResponseHttpComponent, ChartCpuUsageComponent, ChartMemoryUsageComponent],
  imports: [
    CommonModule,
    LayoutModule,
    MatTableModule,
    MatFormFieldModule,
    MatSortModule,
    MatPaginatorModule,
    BrowserModule,
    DxChartModule,
    DxSelectBoxModule

  ],
  exports:[
    TableListingComponent,
    AdminComponent
  ]
})
export class DashboardModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartMemoryUsageComponent } from './chart-memory-usage.component';

describe('ChartMemoryUsageComponent', () => {
  let component: ChartMemoryUsageComponent;
  let fixture: ComponentFixture<ChartMemoryUsageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartMemoryUsageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartMemoryUsageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

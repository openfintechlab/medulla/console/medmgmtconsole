import { Component, OnInit, AfterViewInit,AfterContentInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { serviceMaster } from 'src/app/models/service-master.model';
import { SnackBarService } from 'src/app/services/core/notification-snackbar.service';
import { ServiceMasterDataService } from 'src/app/services/service-master/service-master-data.service';

@Component({
  selector: 'app-service-update',
  templateUrl: './service-update.component.html',
  styleUrls: ['./service-update.component.scss']
})
export class ServiceUpdateComponent implements OnInit {

  selectedValue: string;
  types = [
    {value: 'infra', viewValue: 'Infra'},
    {value: 'functional', viewValue: 'Functional'},
    {value: 'business', viewValue: 'Business'},
    {value: 'technical', viewValue: 'Technical'},
    {value: 'atomic', viewValue: 'Atomic'}
  ];

  implementations = [
    {value: 'typescript', viewValue: 'TypeScript'},
    {value: 'go-lang', viewValue: 'go-lang'},
    {value: 'node', viewValue: 'Node'},
    {value: 'java', viewValue: 'Java'},
    {value: '.net', viewValue: '.Net'},
    {value: 'other', viewValue: 'other'}
  ]
  
  sourceRepo = [
  {value: 'gitlab', viewValue: 'GitLab'},
    {value: 'github', viewValue: 'GitHub'},
    {value: 'other', viewValue: 'other'}
  ]
  tech = [
  {value: 'git', viewValue: 'Git'},
    {value: 'mercurial', viewValue: 'Mercurial'},
    {value: 'other', viewValue: 'other'}
  ]
  lifecycle = [
  {value: 'dev', viewValue: 'Development'},
    {value: 'uat', viewValue: 'User Acceptance Testing'},
    {value: 'sit', viewValue: 'System Integration Testing'},
    {value: 'production', viewValue: 'Production'}
  ]
  visibility = [
  {value: 'private', viewValue: 'Private'},
    {value: 'public', viewValue: 'Public'},
  ]

  serviceMasterUpdateForm: FormGroup;
  private routeSub: Subscription;
  id: any;
  dataSource: any;
  models: any;
  sub: Subscription;
  dCUri: any;
  dcuri1: any;
  dcuri1a: any;
  error: any;
  errorMetadata: string;
  errorTrace: string;
  loading: boolean;
  constructor(
    private fB: FormBuilder,
    private serviceMasterDS: ServiceMasterDataService,
    private ngxUiService: NgxUiLoaderService,
    private router: Router,
    private snackBar:SnackBarService,
    private route: ActivatedRoute
  ) {
    
  }
  
  ngOnInit(): void {
    this.createServiceMasterUpdateForm()
    this.routeSub = this.route.params.subscribe(params => {
      this.id = (params['id']) 
      this.getSingleData()
    });
   

  
    
  }
  ngAfterContentInit(){
    // this.sub = this.getSingleData()
    // .subscribe(son => {
    //   console.log('son', son); // here you get the result
    // });
    console.log(this.dataSource)
  }

  get f() { return this.serviceMasterUpdateForm.controls; }


  createServiceMasterUpdateForm(){
    this.serviceMasterUpdateForm = this.fB.group({
      srv_id: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(56)]],
      serviceName: ['',[Validators.required, Validators.minLength(8), Validators.maxLength(128)]],
      serviceTitle: ['',[Validators.required, Validators.minLength(16), Validators.maxLength(256)]],
      group_slug: ['',[Validators.required, Validators.minLength(8), Validators.maxLength(128)]],
      group_name: ['',[Validators.required, Validators.minLength(8), Validators.maxLength(256)]],
      hostname: ['',[Validators.required, Validators.maxLength(128)]],
      port: ['',[Validators.required, Validators.pattern('^[0-9]*$')]],
      ctx_root_uri: ['',[Validators.maxLength(256)]],
      sdk_doc_uri: ['',[Validators.maxLength(256)]],
      api_doc_uri: ['',[Validators.maxLength(256)]],
      tags: ['',[Validators.maxLength(128)]],
      lifecycle: ['',[Validators.required]],
      type: ['',[Validators.required]],
      visibility: ['',[Validators.required]],
      'imp_tech_uri': this.fB.group({
        type: ['',[Validators.required]],
        version: ['',[Validators.required]],
        isContainerized: ['',[Validators.required]],
      }),
      'source_uri': this.fB.group({
        repo_branch: ['',[Validators.required, Validators.maxLength(256)]],
        repo_url: ['',[Validators.required, Validators.maxLength(256)]],
        repo_tech: ['',[Validators.required, Validators.maxLength(256)]],
        repo_type: ['',[Validators.required, Validators.maxLength(256)]],
      }),
      'discussion_chl_uri': this.fB.group({
        type: ['',[Validators.maxLength(128)]],
        url: ['',[Validators.maxLength(128)]],
      }),
      'service_owner': this.fB.group({
        so_id: ['', ],
        so_service_id: [''],
        so_name: [''],
        so_email_id: [''],
        so_esc_rank: ['', [Validators.min(0), Validators.max(99)]] 
      }),
    
    });
  }


  getSingleData(){
    this.ngxUiService.start();
    this.serviceMasterDS.getSingle(this.id)
      .subscribe(resp => {
          this.ngxUiService.stop();
          this.models = resp.data;
          this.dataSource = resp['service-master'];
          this.dataSource = new serviceMaster(this.dataSource)
          console.log(this.dataSource)
          this.serviceMasterUpdateForm.patchValue(this.dataSource);
          this.serviceMasterUpdateForm.get(['imp_tech_uri','type']).patchValue(this.dataSource.implementedType)
          this.serviceMasterUpdateForm.get(['imp_tech_uri','version']).patchValue(this.dataSource.implementedVersion)
          this.serviceMasterUpdateForm.get(['imp_tech_uri','isContainerized']).patchValue(this.dataSource.isContainerized)
          
          this.serviceMasterUpdateForm.get(['source_uri','repo_branch']).patchValue(this.dataSource.repo_branch)
          this.serviceMasterUpdateForm.get(['source_uri','repo_url']).patchValue(this.dataSource.repo_url)
          this.serviceMasterUpdateForm.get(['source_uri','repo_tech']).patchValue(this.dataSource.repo_tech)
          this.serviceMasterUpdateForm.get(['source_uri','repo_type']).patchValue(this.dataSource.repo_type)
          
          this.serviceMasterUpdateForm.get(['discussion_chl_uri','type']).patchValue(this.dataSource.discussion_chl_type)
          this.serviceMasterUpdateForm.get(['discussion_chl_uri','url']).patchValue(this.dataSource.discussion_chl_url)

        },
        error => {
          this.ngxUiService.stop();
        });
  }
  onSubmit(){
    if(this.serviceMasterUpdateForm.valid){
      this.ngxUiService.start();
      
    const formSubmit = {
      'service-master':{
        'srv_id':this.f.srv_id.value,
        'name':this.f.serviceName.value,
        'title':this.f.serviceTitle.value,
        'group_slug':this.f.group_slug.value,
        'group_name':this.f.group_name.value,
        'imp_tech_uri':`type:${this.serviceMasterUpdateForm.get('imp_tech_uri.type').value};version:${this.serviceMasterUpdateForm.get('imp_tech_uri.version').value};isContainerized:${this.serviceMasterUpdateForm.get('imp_tech_uri.isContainerized').value}`,
        'source_uri': `repo_type:${this.serviceMasterUpdateForm.get('source_uri.repo_type').value};repo_tech:${this.serviceMasterUpdateForm.get('source_uri.repo_tech').value};repo_url:${this.serviceMasterUpdateForm.get('source_uri.repo_url').value};repo_branch:${this.serviceMasterUpdateForm.get('source_uri.repo_branch').value}`,
        'hostname': this.f.hostname.value,
        'port': this.f.port.value,
        'ctx_root_uri': this.f.ctx_root_uri.value,
        'sdk_doc_uri': this.f.sdk_doc_uri.value,
        'api_doc_uri': this.f.api_doc_uri.value,
        'visibility': this.f.visibility.value,
        'type': this.f.type.value,
        'lifecycle': this.f.lifecycle.value,
        'discussion_chl_uri': `type:${this.serviceMasterUpdateForm.get('discussion_chl_uri.type').value};url:${this.serviceMasterUpdateForm.get('discussion_chl_uri.url').value};`,
        'tags': this.f.tags.value,
  
      }
      }
      console.log(formSubmit)
      this.serviceMasterDS.onUpdate(formSubmit)
          .pipe(first())
          .subscribe({
              next: () => {
                  this.ngxUiService.stop()
                  // get return url from route parameters or default to '/'
                  const returnUrl = '/services';
                  this.router.navigate([returnUrl]);
              },
              error: error => {
                  this.error = error;
                  this.errorMetadata = JSON.stringify(error.metadata);
                  this.errorTrace = JSON.stringify(error.metadata.trace[0].description);
                  this.loading = false;
                  this.ngxUiService.stop()
                  console.log(this.error)
                  console.log(this.errorMetadata)
                  this.snackBar.openSnackBar(this.errorTrace,'close');
              }
          });
    }
  }
}

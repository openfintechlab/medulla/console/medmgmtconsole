import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesSidenavComponent } from './services-sidenav.component';

describe('ServicesSidenavComponent', () => {
  let component: ServicesSidenavComponent;
  let fixture: ComponentFixture<ServicesSidenavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicesSidenavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

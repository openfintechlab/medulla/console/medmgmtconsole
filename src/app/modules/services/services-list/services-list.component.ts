import {animate,state,style, transition, trigger} from '@angular/animations';
import {  AfterViewInit,  Component,  ViewChild,  OnInit,  TemplateRef} from '@angular/core';
import {  FormBuilder,  FormGroup} from '@angular/forms';
import {  MatDialog} from '@angular/material/dialog';
import {  MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {  MatTableDataSource} from '@angular/material/table';
import {  RouterOutlet} from '@angular/router';
import {  NgxUiLoaderService} from 'ngx-ui-loader';
import {  first} from 'rxjs/operators';
import {  SnackBarService} from 'src/app/services/core/notification-snackbar.service';
import {  ServiceMasterDataService} from 'src/app/services/service-master/service-master-data.service';




/**
 * @title Service Master List.
 */


@Component({
  selector: 'app-services-list',
  templateUrl: './services-list.component.html',
  styleUrls: ['./services-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({
        height: '0px',
        minHeight: '0'
      })),
      state('expanded', style({
        height: '*'
      })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class ServicesListComponent implements OnInit {

  // : string[] = ['id', 'name', 'progress', 'color', 'type', 'visibility', 'lifecycle'];
  displayedColumns = ['Service Name', 'Port', 'Hostname', 'Title', 'Type', 'Visibility', 'Lifecycle'];
  displayedData = ['name', 'port', 'hostname', 'title', 'type', 'visibility', 'lifecycle'];
  public dataSource: any = {};
  public dataSourceNew: any = SERVICE_VERSION;

  columnsToDisplay = ['Service Name', 'Port', 'Hostname', 'Title', 'Type', 'Visibility', 'Lifecycle'];
  versionColumns = ['version_id', 'srv_id', 'description', 'container_reg_uri']
  expandedElement: PeriodicElement | null;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('addVersionModal') addVersionModal: TemplateRef < any > ;

  models: any;
  public dataSourceV: any = {};
  error: any;
  errorMetadata: string;
  errorTrace: string;

  serviceVersionForm: FormGroup;
  srvMasterId: any;

  constructor(
    private serviceMasterDataService: ServiceMasterDataService,
    private ngxUiService: NgxUiLoaderService,
    private modal: MatDialog,
    private fB: FormBuilder,
    private snackBar: SnackBarService
  ) {

  }
  ngOnInit(): void {
    // throw new Error('Method not implemented.');
    this.getData()
    this.createServiceVersionForm()
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }


  openAddVerModal(id) {
    let modal = this.modal.open(this.addVersionModal);
  }
  closeAddVerModal() {
    let modal = this.modal.closeAll();

  }


  public getData = () => {
    this.ngxUiService.start();
    this.serviceMasterDataService.getAll()
      .subscribe(resp => {
          this.ngxUiService.stop();
          this.models = resp.data;
          this.dataSource = resp['service-master'];
          this.dataSource = new MatTableDataSource < PeriodicElement > (this.dataSource)
          // console.log(this.dataSource.data)
          //  this.dataSource.paginator.length = resp.recordsTotal;
          // this.dataSource.paginator = this.paginator;
        },
        error => {
          this.ngxUiService.stop();
        });
  }

  public getVersions = (id) => {
    this.dataSourceV = {};
    this.srvMasterId = id
    this.ngxUiService.start();
    this.serviceMasterDataService.getSingleVersion(id)
      .subscribe(resp => {
          this.ngxUiService.stop();
          this.models = resp.data;
          this.dataSourceV = resp['service-version'];
          this.dataSourceV = new MatTableDataSource(this.dataSourceV)
          // console.log(this.dataSourceV.data)
          //  this.dataSource.paginator.length = resp.recordsTotal;
          // this.dataSource.paginator = this.paginator;
        },
        error => {
          this.ngxUiService.stop();
        });
  }

  get f() {
    return this.serviceVersionForm.controls;
  }
  // get imp_tech_uri() { return this.serviceMasterForm.controls.imp_tech_uri as FormGroup;  }

  createServiceVersionForm() {
    this.serviceVersionForm = this.fB.group({
      srv_id: [''],
      version_id: [''],
      description: [''],
      container_reg_uri: ['']
    });
  }




  onSubmit() {
    const formSubmit = {
      'service-version': {
        'srv_id': this.srvMasterId,
        'version_id': this.f.version_id.value,
        'description': this.f.description.value,
        'container_reg_uri': this.f.container_reg_uri.value,
      }
    }
    console.log(formSubmit)
    if (this.serviceVersionForm.valid) {
      this.ngxUiService.start();
    }
    console.log(formSubmit)
    this.serviceMasterDataService.onCreateVersion(formSubmit)
      .pipe(first())
      .subscribe({
        next: () => {
          this.getVersions(this.srvMasterId)
          this.ngxUiService.stop()
          this.closeAddVerModal()
          this.createServiceVersionForm()

          // get return url from route parameters or default to '/'
          // const returnUrl = '/services';
          // this.router.navigate([returnUrl]);
        },
        error: error => {
          this.error = error;
          this.errorMetadata = JSON.stringify(error.metadata);
          this.errorTrace = JSON.stringify(error.metadata.trace[0].description);
          // this.loading = false;
          this.ngxUiService.stop()
          console.log(this.error)
          console.log(this.errorMetadata)
          this.snackBar.openSnackBar(this.errorTrace, 'close');
        }
      });
  }
  deleteVersion(ver_id,srv_id){
    if (confirm('Are you sure you want to delete this version?')) {
    this.ngxUiService.start();
    this.serviceMasterDataService.onDeleteVersion(ver_id,srv_id)
      .subscribe(resp => {
          this.getVersions(srv_id)
          this.ngxUiService.stop();

          // console.log(this.dataSourceV.data)
          //  this.dataSource.paginator.length = resp.recordsTotal;
          // this.dataSource.paginator = this.paginator;
        },
        error => {
          this.ngxUiService.stop();
        });
      } else {
        // Do nothing!
        console.log('Thing was not deleted :)');
      }
  }
}

export interface PeriodicElement {
  "Service Name": string;
  Port: number;
  Hostname: string;
  Title: string;
  Type: string;
  Visibility: string;
  Lifecycle: string;
}




const ELEMENT_DATA: PeriodicElement[] = [{

  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"

}, {

  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, {
  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, {
  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, {
  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, {
  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, {
  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, {
  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, {
  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, {
  "Service Name": 'Finance 123',
  Port: 4200,
  Hostname: "Localhost",
  Title: "This is a dummy title",
  Type: "Business",
  Visibility: "Public",
  Lifecycle: "Production"
}, ];

const SERVICE_VERSION = [{
    "Version Id": 'Version 1',
    "Service Id": "826234a8-9519-405a-a1f1-ad991be7e9b0",
    Description: "Alpha Release New",
    "Container Registry URI": "Container Registry URI details"
  },
  {
    "Version Id": 'Version 2',
    "Service Id": "826234a8-9519-405a-a1f1-ad991be7e9b0",
    Description: "Alpha Release New",
    "Container Registry URI": "Container Registry URI details"
  },
  {
    "Version Id": 'Version 3',
    "Service Id": "826234a8-9519-405a-a1f1-ad991be7e9b0",
    Description: "Alpha Release New",
    "Container Registry URI": "Container Registry URI details"
  },
  {
    "Version Id": 'Version 4',
    "Service Id": "826234a8-9519-405a-a1f1-ad991be7e9b0",
    Description: "Alpha Release New",
    "Container Registry URI": "Container Registry URI details"
  },
  {
    "Version Id": 'Version 5',
    "Service Id": "826234a8-9519-405a-a1f1-ad991be7e9b0",
    Description: "Alpha Release New",
    "Container Registry URI": "Container Registry URI details"
  },
  {
    "Version Id": 'Version 6',
    "Service Id": "826234a8-9519-405a-a1f1-ad991be7e9b0",
    Description: "Alpha Release New",
    "Container Registry URI": "Container Registry URI details"
  },
]

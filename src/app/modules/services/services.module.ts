import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from 'src/app/layout/layout.module';
import { ServiceCreateComponent } from './service-create/service-create.component';
import {MatIconModule} from '@angular/material/icon'; 
import {MatSidenavModule} from '@angular/material/sidenav'
import { MatButtonModule } from '@angular/material/button';
import { ServicesListComponent } from './services-list/services-list.component';
import { ServicesSidenavComponent } from './services-sidenav/services-sidenav.component';
import { RouterModule } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelect, MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';
import { ServiceUpdateComponent } from './service-update/service-update.component';
import {MatCardModule} from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [ServiceCreateComponent, ServicesListComponent, ServicesSidenavComponent, ServiceUpdateComponent],
  imports: [
    CommonModule,
    LayoutModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonModule,
    RouterModule,
    MatTableModule,
    MatFormFieldModule,
    MatSortModule,
    MatPaginatorModule,
    MatSelectModule,
    MatInputModule,
    MatCheckboxModule,
    FormsModule,
    MatTabsModule ,
    ReactiveFormsModule,
    MatCardModule,
    MatDialogModule 
    
  ],
  exports:[
    ServiceCreateComponent
  ]
})
export class ServicesModule { }

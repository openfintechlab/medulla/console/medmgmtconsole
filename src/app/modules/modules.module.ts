import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { LoginModule } from './login/login.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ServiceCreateComponent } from './services/service-create/service-create.component';
import { ServicesModule } from './services/services.module';
import { SharedModule } from './shared/shared.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LayoutModule,
    LoginModule,
    DashboardModule,
    ServicesModule,
    SharedModule
  ]
})
export class ModulesModule { }

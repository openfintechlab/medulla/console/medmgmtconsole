import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { SnackBarService } from 'src/app/services/core/notification-snackbar.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('form') form;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  description:string ='';
  errorMetadata: string;
  errorStatus: string;

  constructor(
    private cd: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private ngxUiService: NgxUiLoaderService,
    private snackBar:SnackBarService
  ) {  
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) { 
      this.router.navigate(['/']);
  }
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required,]),
      password: new FormControl('', [Validators.required])
    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit(){
    // console.log(this.loginForm.value)
    this.submitted = true;


    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    
    this.loading = true;
    this.ngxUiService.start();
    this.authenticationService.login(this.f.email.value, this.f.password.value)
        .pipe(first())
        .subscribe({
            next: () => {
                this.ngxUiService.stop()
                // get return url from route parameters or default to '/'
                const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
                this.router.navigate([returnUrl]);
                this.form.resetForm();
            },
            error: error => {
              this.error = error;
              this.errorMetadata = JSON.stringify(error.error.metadata.description);
              this.errorStatus = JSON.stringify(error.error.metadata.status);
              this.errorMetadata = this.errorMetadata.replace(/"/g,'');
              this.errorStatus = this.errorStatus.replace(/"/g,'');
              this.loading = false;
              this.ngxUiService.stop()
              this.snackBar.openSnackBar(`${this.errorStatus}: ${this.errorMetadata}`,'close', 6000, 'notif-error');
              this.form.resetForm();
            }
        });
  }
}
